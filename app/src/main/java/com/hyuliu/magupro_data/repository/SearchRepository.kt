package com.hyuliu.magupro_data.repository

import com.hyuliu.magupro_data.R
import com.hyuliu.magupro_data.kernel.Common_Fragment

class SearchRepository(fragment: Common_Fragment): IRepository {
    private val f = fragment
    val ALL_TYPE = 0
    val BATTER_TYPE = 1
    val PITCHER_TYPE = 2

    fun getTeamList(): Array<String>{
        return f.resources.getStringArray(R.array.team)
    }

    fun getPositionList(): Array<String>{
        return f.resources.getStringArray(R.array.position)
    }

    fun getWeatherList(): Array<String>{
        return f.resources.getStringArray(R.array.weather)
    }

    fun getTypeList(position:Int): Array<String>{
        return when(position){
            0 -> f.resources.getStringArray(R.array.all_type)
            1 -> f.resources.getStringArray(R.array.batter_type)
            2 -> f.resources.getStringArray(R.array.pitcher_type)
            else ->f.resources.getStringArray(R.array.all_type)
        }
    }

}