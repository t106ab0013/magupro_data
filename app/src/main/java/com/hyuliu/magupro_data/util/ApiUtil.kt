package com.hyuliu.magupro_data.util

import java.io.BufferedReader
import java.io.InputStreamReader
import java.io.PrintWriter
import java.net.HttpURLConnection
import java.net.URL
import java.net.URLEncoder

class ApiUtil {
    companion object {
        private val TIME_LIMIT = 5000

        @JvmStatic
        fun getMethod(url: String): String {
            val apiURL = URL(url)
            val connection = apiURL.openConnection() as HttpURLConnection
            try {
                connection.requestMethod = "GET"
                connection.connectTimeout = TIME_LIMIT
                connection.readTimeout = TIME_LIMIT
                if (HttpURLConnection.HTTP_OK == connection.responseCode) {
                    val inputStream = connection.inputStream
                    val reader = BufferedReader(InputStreamReader(inputStream))
                    val response = StringBuilder()
                    reader.forEachLine {
                        response.append(it)
                    }
                    return response.toString()
                }
            } catch (e: Exception) {
                throw e
            } finally {
                connection.disconnect()
            }
            return ""
        }

        @JvmStatic
        fun postMethod(url: String, params: HashMap<String, String>): String {
            val sb = StringBuilder()
            var first = true
            if (params.size == 0) {
                sb.append("")
            } else {
                params.forEach {
                    if (first) {
                        first = false
                        sb.append(URLEncoder.encode(it.key, "UTF-8"))
                        sb.append("=")
                        sb.append(URLEncoder.encode(it.value, "UTF-8"))
                    } else {
                        sb.append("&")
                        sb.append(URLEncoder.encode(it.key, "UTF-8"))
                        sb.append("=")
                        sb.append(URLEncoder.encode(it.value, "UTF-8"))
                    }
                }
            }
            val apiURL = URL(url)
            val connection = apiURL.openConnection() as HttpURLConnection
            try {
                connection.connectTimeout = TIME_LIMIT
                connection.readTimeout = TIME_LIMIT
                connection.requestMethod = "POST"
                connection.setRequestProperty("Content-Type", "application/x-www-form-urlencoded")
                connection.doInput = true
                connection.doOutput = true
                connection.setFixedLengthStreamingMode(sb.toString().length)
                val out = PrintWriter(connection.outputStream)
                out.print(sb.toString())
                out.close()
                if (HttpURLConnection.HTTP_OK == connection.responseCode) {
                    val inputStream = connection.inputStream
                    val reader = BufferedReader(InputStreamReader(inputStream))
                    val response = StringBuilder()
                    reader.forEachLine {
                        response.append(it)
                    }
                    return response.toString()
                }
            } catch (e: Exception) {
                throw e
            } finally {
                connection.disconnect()
            }
            return ""
        }
    }
}