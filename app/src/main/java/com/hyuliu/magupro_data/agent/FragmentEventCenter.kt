package com.hyuliu.magupro_data.agent

import android.os.Bundle
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentTransaction
import com.hyuliu.magupro_data.R
import com.hyuliu.magupro_data.fragment.AboutFragment
import com.hyuliu.magupro_data.fragment.ResultFragment
import com.hyuliu.magupro_data.fragment.SearchFragment
import com.hyuliu.magupro_data.kernel.Common_Fragment

class FragmentEventCenter {

    enum class Name {
        Search,Result,About
    }
    companion object{

        @JvmStatic
        fun ChangeFragment(fragmentManager: FragmentManager, name: Name){
            val bundle = Bundle()
            ChangeFragment(fragmentManager, name,bundle, R.id.main_Layout)
        }

        @JvmStatic
        fun ChangeFragment(fragmentManager: FragmentManager, name: Name, bundle: Bundle){
            ChangeFragment(fragmentManager, name,bundle, R.id.main_Layout)
        }

        @JvmStatic
        fun ChangeFragment(fragmentManager: FragmentManager, name: Name, layoutID: Int){
            val bundle = Bundle()
            ChangeFragment(fragmentManager, name,bundle, layoutID)
        }

        @JvmStatic
        fun ChangeFragment(fragmentManager: FragmentManager, name: Name, bundle: Bundle, layoutID: Int){
            val nowFragment = fragmentManager.findFragmentById(layoutID)
            val transaction: FragmentTransaction = fragmentManager.beginTransaction()
            if (nowFragment != null && nowFragment.tag != null && nowFragment.tag.equals(name.toString())){
                return
            }
            val fragment: Common_Fragment = getFragment(name)
            transaction.replace(layoutID,fragment,name.toString())
            if(bundle.getBoolean("addToBackStack",true)){
                transaction.addToBackStack(name.toString())
            }
            else{
                val count = fragmentManager.backStackEntryCount
                if (count > 0){
                    transaction.addToBackStack(name.toString())
                    fragmentManager.popBackStack()
                }
            }
            fragment.arguments = bundle
            transaction.commitAllowingStateLoss()
        }

        private fun getFragment(name: Name): Common_Fragment{
            return when (name) {
                Name.Search -> SearchFragment()
                Name.Result -> ResultFragment()
                Name.About  -> AboutFragment()
            }
        }
    }
}