package com.hyuliu.magupro_data.viewModel

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.hyuliu.magupro_data.model.Filter

class MainViewModel : ViewModel() {
    val filter by lazy { MutableLiveData<Filter>() }

    init {
        filter.postValue(Filter(0,false,false))
    }

    fun setFilterValue(level:Int,defSpeedOver120:Boolean,runSpeedOver120:Boolean){
        filter.postValue(Filter(level, defSpeedOver120, runSpeedOver120))
    }

}