package com.hyuliu.magupro_data.viewModel

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.hyuliu.magupro_data.common.SystemCommon
import com.hyuliu.magupro_data.common.URL
import com.hyuliu.magupro_data.model.*
import com.hyuliu.magupro_data.repository.ResultRepository
import com.hyuliu.magupro_data.util.ApiUtil
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import org.json.JSONObject

class ResultViewModel(repository: ResultRepository) : ViewModel() {
    private val r = repository

    /**可以Observe的List 用postValue更新*/
    val playerList = MutableLiveData<ArrayList<Player>>()

    /**原始list *唯讀 */
    private val _playerList = ArrayList<Player>()

    /**升級或做其他更動的List*/
    private val filterList = ArrayList<Player>()
    /***/
    private val levelupList = ArrayList<Player>()


    fun startSearch(search: Search?) {
        if (search != null) {
            viewModelScope.launch(Dispatchers.IO) {
                val temp = ApiUtil.postMethod(URL.getURL(URL.Search), parseParam(search))
                withContext(Dispatchers.Main) {
                    parseResult(temp)
                }
            }
        }
    }

    /**
     * --------篩選流程---------
     * 1.先篩選能不能120(如果有勾)
     * 2.算升等後數值
     * 3.更新List
     * ------------------------
     */

    fun setFilter(filter:Filter){
        setBatterCondition(filter.defSpeedOver120,filter.runSpeedOver120)
        setUpGradeLevel(filter.level)
    }

    private fun setUpGradeLevel(level: Int) {
        levelupList.clear()
        filterList.forEach {
            levelupList.add(it.upGrade(level))
        }
        playerList.postValue(levelupList)
    }

    private fun setBatterCondition(defSpeedOver120:Boolean, runSpeedOver120:Boolean){
        filterList.clear()
        _playerList.forEach {
            if (defSpeedOver120){
                if (it is Batter && it.isTopDefSpeed()){
                    filterList.add(it)
                }
            }
            if (runSpeedOver120){
                /**避免兩項都符合的重複加入*/
                if (it is Batter && it.isTopRunSpeed() && !filterList.contains(it)){
                    filterList.add(it)
                }
            }

            //都沒有勾的時候，不篩選
            if (!defSpeedOver120 && !runSpeedOver120){
                filterList.add(it)
            }
        }
    }

    private fun parseParam(search: Search): HashMap<String, String> {
        val map = HashMap<String, String>()
        if (search.name.isNotEmpty()) map["name"] = "%${search.name}%" else map["name"] = "%"
        if (search.team != "所有球團") map["team"] = search.team else map["team"] = "%"
        if (search.position != "所有位置") map["position"] =
            PlayerDataMap.getPositionAbbr(search.position) else map["position"] = "%"
        map["ovrMin"] = search.ovrMin.toString()
        map["ovrMax"] = search.ovrMax.toString()
        map["yearMin"] = search.yearMin.toString()
        map["yearMax"] = search.yearMax.toString()
        if (search.weather != "所有天氣") map["weather"] = search.weather else map["weather"] = "%"
        if (search.type != "所有類型") map["type"] = search.type else map["type"] = "%"
        return map
    }

    private fun parseResult(data: String?) {
        if (data != null) {
            try {
                val jsonObject = JSONObject(data)
                val pitcherArray = jsonObject.getJSONArray("Pitcher")
                if (pitcherArray.length() > 0) {
                    for (i in 0 until pitcherArray.length()) {
                        val item: JSONObject = pitcherArray.getJSONObject(i)
                        val pitcher = Pitcher(
                            item.getInt("year"),
                            item.getString("name"),
                            item.getString("position"),
                            item.getInt("ovr"),
                            item.getString("team"),
                            item.getString("threw"),
                            item.getString("hit"),
                            item.getString("weather"),
                            item.getString("type"),
                            item.getInt("stamina"),
                            item.getInt("control"),
                            item.getInt("speed"),
                            item.getString("BallType1"),
                            item.getInt("BallType1Val"),
                            item.getString("BallType2"),
                            item.getInt("BallType2Val"),
                            item.getString("BallType3"),
                            item.getInt("BallType3Val"),
                            item.getString("BallType4"),
                            item.getInt("BallType4Val"),
                            item.getString("BallType5"),
                            item.getInt("BallType5Val"),
                            item.getString("BallType6"),
                            item.getInt("BallType6Val")
                        )
                        _playerList.add(pitcher)
                    }
                }
                val batterArray = jsonObject.getJSONArray("Batter")
                if (batterArray.length() > 0) {
                    for (i in 0 until batterArray.length()) {
                        val item: JSONObject = batterArray.getJSONObject(i)
                        val batter = Batter(
                            item.getInt("year"),
                            item.getString("name"),
                            item.getString("position"),
                            item.getInt("ovr"),
                            item.getString("team"),
                            item.getString("threw"),
                            item.getString("hit"),
                            item.getString("weather"),
                            item.getString("type"),
                            item.getInt("power"),
                            item.getInt("contact"),
                            item.getInt("runspeed"),
                            item.getInt("defspeed"),
                            item.getInt("defrange"),
                            item.getInt("threwpower"),
                            item.getInt("threwskill"),
                            item.getInt("agile")
                        )
                        _playerList.add(batter)
                    }
                }
                SystemCommon.PlayerListSort(_playerList)
                playerList.postValue(_playerList)
            } catch (e: Exception) {
                throw e
            }
        }
    }


}