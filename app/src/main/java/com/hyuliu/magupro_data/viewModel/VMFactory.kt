package com.hyuliu.magupro_data.viewModel

import android.app.Application
import androidx.annotation.NonNull
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.ViewModelStoreOwner
import com.hyuliu.magupro_data.kernel.Common_Fragment
import com.hyuliu.magupro_data.repository.IRepository
import com.hyuliu.magupro_data.repository.ResultRepository
import com.hyuliu.magupro_data.repository.SearchRepository

class VMFactory constructor(val application: Application,private val repository: IRepository?) : ViewModelProvider.AndroidViewModelFactory(application) {

    companion object{
        @JvmStatic
        fun <T: ViewModel> viewModel(fragment: Common_Fragment, clazz: Class<T>, data: IRepository? = null): T{
            return viewModel(
                fragment,
                fragment.requireActivity().application!!,
                clazz,
                data
            )
        }

        @JvmStatic
        fun <T:ViewModel> viewModel(activity: AppCompatActivity, clazz: Class<T>, data: IRepository? = null):T{
            return viewModel(
                activity,
                activity.application,
                clazz,
                data
            )
        }

        @JvmStatic
        fun <T: ViewModel> viewModel(vmStoreOwner: ViewModelStoreOwner,@NonNull application: Application, clazz: Class<T>, data: IRepository? = null):T {
            return ViewModelProvider(vmStoreOwner,
                VMFactory(application, data)
            ).get(clazz)
        }
    }

    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        return when{
            modelClass.isAssignableFrom(SearchViewModel::class.java) -> SearchViewModel(repository as SearchRepository) as T
            modelClass.isAssignableFrom(ResultViewModel::class.java) -> ResultViewModel(repository as ResultRepository) as T
            modelClass.isAssignableFrom(MainViewModel::class.java) -> MainViewModel() as T
            else -> super.create(modelClass)
        }
    }
}