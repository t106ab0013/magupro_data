package com.hyuliu.magupro_data.viewModel

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.hyuliu.magupro_data.common.URL
import com.hyuliu.magupro_data.model.Search
import com.hyuliu.magupro_data.repository.SearchRepository
import com.hyuliu.magupro_data.util.ApiUtil
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import org.json.JSONObject

class SearchViewModel(repository: SearchRepository): ViewModel() {
    private val r = repository

    val name = MutableLiveData<String>()
    val team = MutableLiveData<String>()
    val position = MutableLiveData<String>()
    val ovrMin = MutableLiveData<Int>()
    val ovrMax = MutableLiveData<Int>()
    val yearMin = MutableLiveData<Int>()
    val yearMax = MutableLiveData<Int>()
    val weather = MutableLiveData<String>()
    val type = MutableLiveData<String>()
    var typelisttype:Int = 0
    private lateinit var teamArray:Array<String>

    val seekBarInitValue = MutableLiveData<ArrayList<Int>>()

    init {
        name.value = ""
        team.value = r.getTeamList()[0]
        position.value = r.getPositionList()[0]
        weather.value = r.getWeatherList()[0]
        type.value = r.getTypeList(r.ALL_TYPE)[0]
        val tempList = ArrayList<Int>()
        tempList.add(99)
        tempList.add(56)
        tempList.add(2021)
        tempList.add(1990)
        seekBarInitValue.postValue(tempList)
        teamArray = Array(1){"所有球團"}
    }

    fun setName(n:String){
        if (n.isEmpty()){ name.value = "" }
        else{ name.value = n }
    }

    fun setTeam(t:String){
        team.value = t
    }

    fun setPosition(p:String){
        position.value = p
    }

    fun setOVR(min:Int,max:Int){
        ovrMin.value = min
        ovrMax.value = max
    }

    fun setYear(min:Int,max:Int){
        yearMin.value = min
        yearMax.value = max
    }

    fun setWeather(w:String){
        weather.value = w
    }

    fun setType(t:String){
        type.value = t
    }

    fun setTypeListType(i:Int){
        typelisttype = i
    }

    fun getSearch() :Search {
        return Search(name.value.toString(),team.value.toString(),position.value.toString(),
            ovrMin.value!!.toInt(),ovrMax.value!!.toInt(),yearMin.value!!.toInt(),yearMax.value!!.toInt(),
            weather.value.toString(),type.value.toString())
    }

    fun getBasicValue(){
        viewModelScope.launch(Dispatchers.IO){
            val data = ApiUtil.getMethod(URL.getURL(URL.BasicValue))
            withContext(Dispatchers.Main){
                parseResult(data)
            }
        }
    }

    private fun parseResult(data:String){
        val jsonObject = JSONObject(data)
        val array = jsonObject.getJSONArray("Team")
        teamArray = Array(array.length()+1){""}
        teamArray[0] = "所有球團"
        for (i in 0 until array.length()){
            teamArray[i+1] = array.getString(i)
        }
        val tempList = ArrayList<Int>()
        tempList.add(jsonObject.getInt("MaxOVR"))
        tempList.add(jsonObject.getInt("MinOVR"))
        tempList.add(jsonObject.getInt("MaxYear"))
        tempList.add(jsonObject.getInt("MinYear"))
        seekBarInitValue.postValue(tempList)
    }

    fun getTeamList(): Array<String>{
        return teamArray
    }

    fun getPositionList(): Array<String>{
        return r.getPositionList()
    }

    fun getWeatherList(): Array<String>{
        return r.getWeatherList()
    }

    fun getTypeList(): Array<String>{
        return when(typelisttype){
            0 -> r.getTypeList(r.ALL_TYPE)
            in 1..9 -> r.getTypeList(r.BATTER_TYPE)
            else -> r.getTypeList(r.PITCHER_TYPE)
        }
    }

}




