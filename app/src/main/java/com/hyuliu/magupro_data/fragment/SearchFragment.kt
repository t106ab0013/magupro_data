package com.hyuliu.magupro_data.fragment

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.*
import androidx.appcompat.app.AlertDialog
import androidx.lifecycle.Observer
import com.hyuliu.magupro_data.R
import com.hyuliu.magupro_data.agent.FragmentEventCenter
import com.hyuliu.magupro_data.kernel.Common_Fragment
import com.hyuliu.magupro_data.repository.SearchRepository
import com.hyuliu.magupro_data.viewModel.SearchViewModel
import com.hyuliu.magupro_data.viewModel.VMFactory
import com.jaygoo.widget.OnRangeChangedListener
import com.jaygoo.widget.RangeSeekBar

class SearchFragment : Common_Fragment(), View.OnClickListener {
    lateinit var ovrBar: RangeSeekBar
    lateinit var yearBar: RangeSeekBar
    lateinit var tvTeam: TextView
    lateinit var tvPosition: TextView
    lateinit var tvWeather: TextView
    lateinit var tvType: TextView
    lateinit var nameClear: ImageView
    lateinit var etName: EditText
    lateinit var btnSearch: Button

    private lateinit var viewModel: SearchViewModel

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val v = inflater.inflate(R.layout.fragment_search, container, false)

        viewModel = VMFactory.viewModel(this, SearchViewModel::class.java, SearchRepository(this))
        if (isNetworkConnected()) {
            viewModel.getBasicValue()
        } else {
            Toast.makeText(mainActivity, "無法連接網路", Toast.LENGTH_SHORT).show()
        }
        ovrBar = v.findViewById(R.id.ovrBar)
        yearBar = v.findViewById(R.id.yearBar)
        tvTeam = v.findViewById(R.id.tvTeam)
        tvPosition = v.findViewById(R.id.tvPosition)
        tvWeather = v.findViewById(R.id.tvWeather)
        tvType = v.findViewById(R.id.tvType)
        nameClear = v.findViewById(R.id.nameClear)
        etName = v.findViewById(R.id.etName)
        btnSearch = v.findViewById(R.id.btnSearch)
        setObserver()

        nameClear.setOnClickListener(this)
        tvTeam.setOnClickListener(this)
        tvPosition.setOnClickListener(this)
        tvWeather.setOnClickListener(this)
        tvType.setOnClickListener(this)
        btnSearch.setOnClickListener(this)

        return v
    }

    override fun onStart() {
        super.onStart()
        mainActivity.setbtnBackVisibility(View.GONE)
        mainActivity.setTopbarTitle("球員數據查詢APP")
    }

    private fun setObserver() {
        viewModel.team.observe(viewLifecycleOwner, Observer { tvTeam.text = it })
        viewModel.position.observe(viewLifecycleOwner, Observer { tvPosition.text = it })
        viewModel.weather.observe(viewLifecycleOwner, Observer { tvWeather.text = it })
        viewModel.type.observe(viewLifecycleOwner, Observer { tvType.text = it })
        viewModel.seekBarInitValue.observe(viewLifecycleOwner, {
            ovrBar.setRange(it[1].toFloat(), it[0].toFloat())
            yearBar.setRange(it[3].toFloat(), it[2].toFloat())
            setSeekBars(it[0], it[1], it[2], it[3])
        })
    }

    override fun onClick(v: View?) {
        when (v?.id) {
            R.id.btnSearch -> {
                if (mainActivity.isNetworkConnected()) {
                    viewModel.setName(etName.text.toString())
                    viewModel.setTeam(tvTeam.text.toString())
                    viewModel.setPosition(tvPosition.text.toString())
                    viewModel.setOVR(
                        ovrBar.leftSeekBar.progress.toInt(),
                        ovrBar.rightSeekBar.progress.toInt()
                    )
                    viewModel.setYear(
                        yearBar.leftSeekBar.progress.toInt(),
                        yearBar.rightSeekBar.progress.toInt()
                    )
                    viewModel.setWeather(tvWeather.text.toString())
                    viewModel.setType(tvType.text.toString())

                    FragmentEventCenter.ChangeFragment(
                        activity?.supportFragmentManager!!,
                        FragmentEventCenter.Name.Result,
                        Bundle().apply {
                            putParcelable("search", viewModel.getSearch())
                        })
                } else {
                    Toast.makeText(context, "請確認網路連接!!!", Toast.LENGTH_SHORT).show()
                    return
                }
            }
            R.id.nameClear -> {
                etName.text.clear()
            }
            R.id.tvTeam -> {
                AlertDialog.Builder(requireContext())
                    .setItems(viewModel.getTeamList()) { dialog, which ->
                        viewModel.setTeam(
                            viewModel.getTeamList()[which]
                        )
                    }
                    .show()
            }
            R.id.tvPosition -> {
                AlertDialog.Builder(requireContext())
                    .setItems(viewModel.getPositionList()) { dialog, which ->
                        viewModel.setPosition(viewModel.getPositionList()[which])
                        viewModel.setTypeListType(which)
                        viewModel.setType(viewModel.getTypeList()[0])
                    }
                    .show()
            }
            R.id.tvWeather -> {
                AlertDialog.Builder(requireContext())
                    .setItems(viewModel.getWeatherList()) { dialog, which ->
                        viewModel.setWeather(
                            viewModel.getWeatherList()[which]
                        )
                    }
                    .show()
            }
            R.id.tvType -> {
                AlertDialog.Builder(requireContext())
                    .setItems(viewModel.getTypeList()) { dialog, which ->
                        viewModel.setType(
                            viewModel.getTypeList()[which]
                        )
                    }
                    .show()
            }

        }
    }

    private fun setSeekBars(ovrMax: Int, ovrMin: Int, yearMax: Int, yearMin: Int) {
        ovrBar.setIndicatorTextDecimalFormat("0")
        ovrBar.invalidate()
        ovrBar.setProgress(ovrMin.toFloat(), ovrMax.toFloat())
        ovrBar.setOnRangeChangedListener(object : OnRangeChangedListener {
            override fun onStartTrackingTouch(view: RangeSeekBar?, isLeft: Boolean) {

            }

            override fun onRangeChanged(
                view: RangeSeekBar?,
                leftValue: Float,
                rightValue: Float,
                isFromUser: Boolean
            ) {

            }

            override fun onStopTrackingTouch(view: RangeSeekBar?, isLeft: Boolean) {

            }
        })
        yearBar.setIndicatorTextDecimalFormat("0")
        yearBar.invalidate()
        yearBar.setProgress(yearMin.toFloat(), yearMax.toFloat())
        yearBar.setOnRangeChangedListener(object : OnRangeChangedListener {
            override fun onStartTrackingTouch(view: RangeSeekBar?, isLeft: Boolean) {

            }

            override fun onRangeChanged(
                view: RangeSeekBar?,
                leftValue: Float,
                rightValue: Float,
                isFromUser: Boolean
            ) {

            }

            override fun onStopTrackingTouch(view: RangeSeekBar?, isLeft: Boolean) {
            }
        })
    }

}
