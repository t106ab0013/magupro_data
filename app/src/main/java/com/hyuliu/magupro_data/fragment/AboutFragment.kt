package com.hyuliu.magupro_data.fragment

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.hyuliu.magupro_data.R
import com.hyuliu.magupro_data.kernel.Common_Fragment

class AboutFragment : Common_Fragment() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        val v = inflater.inflate(R.layout.fragment_about, container, false)

        return v
    }

    override fun onStart() {
        super.onStart()
        mainActivity.setBtnMoreVisibility(View.INVISIBLE)
        mainActivity.setTopbarTitle("關於APP")
    }
}