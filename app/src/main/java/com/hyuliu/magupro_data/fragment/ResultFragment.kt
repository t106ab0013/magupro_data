package com.hyuliu.magupro_data.fragment

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.ViewTreeObserver
import android.widget.TextView
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.hyuliu.magupro_data.R
import com.hyuliu.magupro_data.adapter.PlayerResultAdapter
import com.hyuliu.magupro_data.kernel.Common_Fragment
import com.hyuliu.magupro_data.repository.ResultRepository
import com.hyuliu.magupro_data.viewModel.MainViewModel
import com.hyuliu.magupro_data.viewModel.ResultViewModel
import com.hyuliu.magupro_data.viewModel.VMFactory


class ResultFragment : Common_Fragment() {
    private val viewModel: ResultViewModel by lazy {
        VMFactory.viewModel(
            this,
            ResultViewModel::class.java,
            ResultRepository(context, requireArguments())
        )
    }
    private val mainViewModel: MainViewModel by lazy {
        VMFactory.viewModel(
            mainActivity,
            MainViewModel::class.java
        )
    }
    private lateinit var recyclerView: RecyclerView
    private lateinit var tvNotFound: TextView
    private val adapter: PlayerResultAdapter by lazy {
        PlayerResultAdapter(
            mainActivity,
            viewModel
        )
    }
    private var count = 0

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setObserver()
        viewModel.startSearch(requireArguments().getParcelable("search"))
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val v = inflater.inflate(R.layout.fragment_result, container, false)
        recyclerView = v.findViewById(R.id.recyclerview)
        tvNotFound = v.findViewById(R.id.tv_notfound)
        tvNotFound.visibility = View.GONE
        setRecyclerView()
        return v
    }

    override fun onStart() {
        super.onStart()
        mainActivity.setTopbarTitle("搜尋結果")
        setBtnSettingVisibility(View.VISIBLE)
    }

    private fun setObserver() {
        viewModel.playerList.observe(this, {
            /**
             * 第一次觀察到的list更新是空值，設定若第二次更新再沒有值就跳提示
             */
            showLoadingDialog()
            if (it.size > 0) {
                updateRecyclerView()
            } else {
                count++
                if (count > 1) {
                    tvNotFound.visibility = View.VISIBLE
                    recyclerView.visibility = View.GONE
                    dismissLoadingDialog()
                }
            }
        })
        mainViewModel.filter.observe(this,{
            viewModel.setFilter(it)
        })
    }

    private fun updateRecyclerView() {
        recyclerView.visibility = View.VISIBLE
        tvNotFound.visibility = View.GONE
        adapter.notifyDataSetChanged()
        recyclerView.runWhenReady {
            dismissLoadingDialog()
        }
    }

    private fun setRecyclerView() {
        recyclerView.adapter = adapter
        recyclerView.layoutManager = LinearLayoutManager(this.activity)
    }

    private fun RecyclerView.runWhenReady(action: () -> Unit) {
        val globalLayoutListener = object : ViewTreeObserver.OnGlobalLayoutListener {
            override fun onGlobalLayout() {
                action()
                viewTreeObserver.removeOnGlobalLayoutListener(this)
            }
        }
        viewTreeObserver.addOnGlobalLayoutListener(globalLayoutListener)
    }

}