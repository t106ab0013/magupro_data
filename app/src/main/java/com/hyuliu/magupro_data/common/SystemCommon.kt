package com.hyuliu.magupro_data.common

import com.hyuliu.magupro_data.model.Player

class SystemCommon {
    companion object {
        @JvmStatic
        fun PlayerListSort(list: ArrayList<Player>) {
            list.sortByDescending { it.getPlayerOVR() }
        }

    }
}