package com.hyuliu.magupro_data.common


class URL {
    companion object{
        private const val root = "http://maguproject.lionfree.net/"

        const val Search = "SEARCH_ALL.php"
        const val BasicValue = "Basic_Value.php"

        fun getURL(tag:String):String{
            return root + tag
        }

    }
}