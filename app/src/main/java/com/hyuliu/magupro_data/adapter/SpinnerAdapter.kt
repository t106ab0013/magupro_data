package com.hyuliu.magupro_data.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.BaseAdapter
import android.widget.TextView
import com.hyuliu.magupro_data.R

class SpinnerAdapter(val context: Context, private val array:Array<String>): BaseAdapter(){

    private val inflater = context.getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater

    override fun getCount(): Int {
        return array.size
    }

    override fun getItem(p0: Int): Any {
        return array[p0]
    }

    override fun getItemId(p0: Int): Long {
        return p0.toLong()
    }

    override fun getView(p0: Int, p1: View?, p2: ViewGroup?): View {
        val view:View
        val viewHolder:ItemViewHolder
        if (p1 == null){
            view = inflater.inflate(R.layout.spinner_dropdown_layout,p2,false)
            viewHolder = ItemViewHolder(view)
            view.tag = viewHolder
        }
        else{
            view = p1
            viewHolder = view.tag as ItemViewHolder
        }
        viewHolder.tvLevel.text = array[p0]

        return view
    }

    private class ItemViewHolder(item:View) {
        val tvLevel:TextView = item.findViewById(R.id.tv_level)
    }

}