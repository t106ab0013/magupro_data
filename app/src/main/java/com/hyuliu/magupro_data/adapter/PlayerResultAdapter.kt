package com.hyuliu.magupro_data.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.RecyclerView
import com.hyuliu.magupro_data.R
import com.hyuliu.magupro_data.model.Player
import com.hyuliu.magupro_data.model.PlayerDataMap
import com.hyuliu.magupro_data.viewModel.ResultViewModel
import com.hyuliu.magupro_data.widget.AbilityTextView

class PlayerResultAdapter(val context: Context, val viewModel: ResultViewModel) :
    RecyclerView.Adapter<PlayerResultAdapter.PlayerResultViewHolder>() {

    inner class PlayerResultViewHolder(item: View) : RecyclerView.ViewHolder(item) {
        private val tvName: TextView = item.findViewById(R.id.tv_name)
        private val tvTeam: TextView = item.findViewById(R.id.tv_team)
        private val tvWeather:TextView = item.findViewById(R.id.tv_weather)
        private val tvPosition: TextView = item.findViewById(R.id.tv_position)
        private val tvOvr: TextView = item.findViewById(R.id.tv_ovr)
        private val tvAb1: TextView = item.findViewById(R.id.tv_ab1)
        private val tvAb2: TextView = item.findViewById(R.id.tv_ab2)
        private val tvAb3: TextView = item.findViewById(R.id.tv_ab3)
        private val tvAb4: TextView = item.findViewById(R.id.tv_ab4)
        private val tvAb5: TextView = item.findViewById(R.id.tv_ab5)
        private val tvAb6: TextView = item.findViewById(R.id.tv_ab6)
        private val tvAb7: TextView = item.findViewById(R.id.tv_ab7)
        private val tvAb8: TextView = item.findViewById(R.id.tv_ab8)
        private val tvAb9: TextView = item.findViewById(R.id.tv_ab9)
        private val tvValue1: AbilityTextView = item.findViewById(R.id.tv_value1)
        private val tvValue2: AbilityTextView = item.findViewById(R.id.tv_value2)
        private val tvValue3: AbilityTextView = item.findViewById(R.id.tv_value3)
        private val tvValue4: AbilityTextView = item.findViewById(R.id.tv_value4)
        private val tvValue5: AbilityTextView = item.findViewById(R.id.tv_value5)
        private val tvValue6: AbilityTextView = item.findViewById(R.id.tv_value6)
        private val tvValue7: AbilityTextView = item.findViewById(R.id.tv_value7)
        private val tvValue8: AbilityTextView = item.findViewById(R.id.tv_value8)
        private val tvValue9: AbilityTextView = item.findViewById(R.id.tv_value9)

        fun bind(player: Player) {
            tvName.text = "${player.getPlayerYear()}  ${player.getPlayerName()}"
            when (player.getPlayerOVR()) {
                in 56..70 -> tvName.setTextColor(ContextCompat.getColor(context, R.color.card_blue))
                in 71..75 -> tvName.setTextColor(
                    ContextCompat.getColor(
                        context,
                        R.color.card_yellow
                    )
                )
                in 76..80 -> tvName.setTextColor(ContextCompat.getColor(context, R.color.card_red))
                in 81..99 -> tvName.setTextColor(
                    ContextCompat.getColor(
                        context,
                        R.color.card_purple
                    )
                )
            }
            tvTeam.text = player.getPlayerTeam()
            tvPosition.text = player.getPlayerPosition()
            tvWeather.text = player.getWeatherType()
            tvOvr.text = player.getPlayerOVR().toString()
            tvAb1.text = player.getAbilityName1()
            tvAb2.text = player.getAbilityName2()
            tvAb3.text = player.getAbilityName3()
            tvAb4.text = player.getAbilityName4()
            tvAb5.text = player.getAbilityName5()
            tvAb6.text = player.getAbilityName6()
            tvAb7.text = player.getAbilityName7()
            tvAb8.visibility = View.VISIBLE
            tvAb9.visibility = View.VISIBLE
            tvValue8.visibility = View.VISIBLE
            tvValue9.visibility = View.VISIBLE
            if (player.getAbilityName8() == PlayerDataMap.NONE) {
                tvAb8.visibility = View.INVISIBLE
                tvValue8.visibility = View.INVISIBLE
            } else tvAb8.text = player.getAbilityName8()
            if (player.getAbilityName9() == PlayerDataMap.NONE) {
                tvAb9.visibility = View.INVISIBLE
                tvValue9.visibility = View.INVISIBLE
            } else tvAb9.text = player.getAbilityName9()
            setAbilityTextView(tvValue1, player.getAbilityValue1())
            setAbilityTextView(tvValue2, player.getAbilityValue2())
            setAbilityTextView(tvValue3, player.getAbilityValue3())
            setAbilityTextView(tvValue4, player.getAbilityValue4())
            setAbilityTextView(tvValue5, player.getAbilityValue5())
            setAbilityTextView(tvValue6, player.getAbilityValue6())
            setAbilityTextView(tvValue7, player.getAbilityValue7())
            setAbilityTextView(tvValue8, player.getAbilityValue8())
            setAbilityTextView(tvValue9, player.getAbilityValue9())
        }
    }

    fun setAbilityTextView(textview: AbilityTextView, value: String) {
        textview.text = value
        textview.setTextColor(PlayerDataMap.getAbilityColor(context, value))
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): PlayerResultViewHolder {
        return PlayerResultViewHolder(
            LayoutInflater.from(context).inflate(R.layout.search_result_v2, parent, false)
        )
    }

    override fun getItemCount(): Int {
        return if (viewModel.playerList.value == null) {
            0
        } else {
            viewModel.playerList.value!!.size
        }

    }

    override fun onBindViewHolder(holder: PlayerResultViewHolder, position: Int) {
        if (viewModel.playerList.value != null) {
            holder.bind(viewModel.playerList.value!![position])
        }
    }


}