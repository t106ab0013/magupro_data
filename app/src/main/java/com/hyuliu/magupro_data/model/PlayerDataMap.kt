package com.hyuliu.magupro_data.model

import android.content.Context
import androidx.core.content.ContextCompat
import com.hyuliu.magupro_data.R

class PlayerDataMap {
    companion object {
        const val BATTER = "B"
        const val PITCHER = "P"
        const val NONE = "無"

        fun getPositionAbbr(p: String): String {
            return when (p) {
                "指定打擊 (DH)" -> "DH"
                "捕手 (C)" -> "C"
                "一壘手 (1B)" -> "1B"
                "二壘手 (2B)" -> "2B"
                "三壘手 (3B)" -> "3B"
                "游擊手 (SS)" -> "SS"
                "左外野手 (LF)" -> "LF"
                "中外野手 (CF)" -> "CF"
                "右外野手 (RF)" -> "RF"
                "先發 (SP)" -> "SP"
                "後援 (RP/CP)" -> "RP||CP"
                else -> "'"
            }
        }

        fun getAbilityAbbr(value:String): String {
            return when(value){
                "跑壘速度" -> "跑速"
                "守備速度" -> "守速"
                "守備範圍" -> "守範"
                "傳球力量" -> "傳力"
                "傳球技能" -> "傳技"
                "控球能力" -> "控球"
                "R快速" -> "R快"
                "變速球" -> "變速"
                "C變速球" -> "C變"
                "S曲球" -> "S曲"
                "K曲球" -> "K曲"
                "V滑球" -> "V滑"
                "V二縫" -> "V二"
                "H二縫" -> "H二"
                "指叉球" -> "指叉"
                "伸卡球" -> "伸卡"
                "快速指叉球" -> "快叉"
                "螺旋球" -> "螺旋"
                "掌心球" -> "掌心"
                "蝴蝶球" -> "蝴蝶"
                "滑曲球" -> "滑曲"
                "12-6曲球" -> "12-6曲"
                "力量曲球" -> "力曲"
                else -> value
            }
        }

        fun getAbilityColor(context: Context, text: String): Int {
            return when (text.toInt()) {
                in 0..70 -> {
                    ContextCompat.getColor(context,R.color.color_70)
                }
                in 71..80 -> {
                    ContextCompat.getColor(context,R.color.color_7180)
                }
                in 81..90 -> {
                    ContextCompat.getColor(context,R.color.color_8190)
                }
                in 91..100 -> {
                    ContextCompat.getColor(context,R.color.color_91100)
                }
                in 101..110 -> {
                    ContextCompat.getColor(context,R.color.color_101100)
                }
                in 111..120 -> {
                    ContextCompat.getColor(context,R.color.color_111120)
                }
                in 121..130 -> {
                    ContextCompat.getColor(context,R.color.color_121130)
                }
                in 131..140 -> {
                    ContextCompat.getColor(context,R.color.color_131140)
                }
                else -> ContextCompat.getColor(context,R.color.color_70)
            }

        }

    }
}