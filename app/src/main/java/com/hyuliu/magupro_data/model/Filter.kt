package com.hyuliu.magupro_data.model

data class Filter(
    val level:Int,
    val defSpeedOver120:Boolean,
    val runSpeedOver120:Boolean
)
