package com.hyuliu.magupro_data.model

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
data class Search (var name:String, val team:String, val position:String, val ovrMin:Int, val ovrMax:Int, val yearMin:Int, val yearMax:Int, val weather:String, val type:String) : Parcelable