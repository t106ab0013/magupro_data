package com.hyuliu.magupro_data.model

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
data class Pitcher(
    val year: Int,
    val name: String,
    val position: String,
    val ovr: Int,
    val team: String,
    val threw: String,
    val hit: String,
    val weather: String,
    val type: String,
    val stamina: Int,
    val control: Int,
    val speed: Int,
    val BallType1: String,
    val BallType1Val: Int,
    val BallType2: String,
    val BallType2Val: Int,
    val BallType3: String,
    val BallType3Val: Int,
    val BallType4: String,
    val BallType4Val: Int,
    val BallType5: String,
    val BallType5Val: Int,
    val BallType6: String,
    val BallType6Val: Int
) : Parcelable, Player {

    override fun getPlayerTeam(): String {
        return team
    }

    override fun getPlayerName(): String {
        return name
    }

    override fun getPlayerYear(): String {
        return year.toString().substring(2)
    }

    override fun getPlayerPosition(): String {
        return position
    }

    override fun getPlayerOVR(): Int {
        return ovr
    }

    override fun getPlayerBP(): String {
        return PlayerDataMap.PITCHER
    }

    override fun getWeatherType(): String {
        return weather
    }

    override fun getAbilityName1(): String {
        return "體力"
    }

    override fun getAbilityName2(): String {
        return "控球"
    }

    override fun getAbilityName3(): String {
        return "球速"
    }

    override fun getAbilityName4(): String {
        return PlayerDataMap.getAbilityAbbr(BallType1)
    }

    override fun getAbilityName5(): String {
        return PlayerDataMap.getAbilityAbbr(BallType2)
    }

    override fun getAbilityName6(): String {
        return PlayerDataMap.getAbilityAbbr(BallType3)
    }

    override fun getAbilityName7(): String {
        return PlayerDataMap.getAbilityAbbr(BallType4)
    }

    override fun getAbilityName8(): String {
        return PlayerDataMap.getAbilityAbbr(BallType5)
    }

    override fun getAbilityName9(): String {
        return PlayerDataMap.getAbilityAbbr(BallType6)
    }

    override fun getAbilityValue1(): String {
        return stamina.toString()
    }

    override fun getAbilityValue2(): String {
        return control.toString()
    }

    override fun getAbilityValue3(): String {
        return speed.toString()
    }

    override fun getAbilityValue4(): String {
        return BallType1Val.toString()
    }

    override fun getAbilityValue5(): String {
        return BallType2Val.toString()
    }

    override fun getAbilityValue6(): String {
        return BallType3Val.toString()
    }

    override fun getAbilityValue7(): String {
        return BallType4Val.toString()
    }

    override fun getAbilityValue8(): String {
        return BallType5Val.toString()
    }

    override fun getAbilityValue9(): String {
        return BallType6Val.toString()
    }

    override fun upGrade(level: Int): Player {
        /**
         * 1.體力不會提升
         * 2.控球、球速提升不分類型
         * 3.直球系(四縫、R快)提升不分類型
         * 4.升級類型中的球路只會在能力值出現一種
         * 5.紫卡(OVR>=81)提升幅度不同
         */
        val elseAdd: IntArray
        val subAdd: IntArray
        val mainAdd = arrayOf(0, 1, 2, 4, 6, 8, 10, 13, 16, 20, 25)
        if (getPlayerOVR() >= 81) {
            subAdd = intArrayOf(0, 1, 2, 4, 5, 7, 8, 9, 11, 13, 17)
            elseAdd = intArrayOf(0, 1, 2, 4, 5, 7, 8, 9, 11, 13, 17)
        } else {
            subAdd = intArrayOf(0, 1, 2, 3, 4, 5, 6, 7, 8, 10, 13)
            elseAdd = intArrayOf(0, 0, 1, 2, 3, 4, 6, 7, 8, 10, 13)
        }
        val abilityList = LinkedHashMap<String, Int>()
        abilityList[BallType2] = BallType2Val
        abilityList[BallType3] = BallType3Val
        abilityList[BallType4] = BallType4Val
        abilityList[BallType5] = BallType5Val
        abilityList[BallType6] = BallType6Val
        val addList = LinkedHashMap<String, Int>()

        abilityList.forEach {
            when (type) {
                "變速球型" -> {
                    if (it.key == "變速球" || it.key == "C變速球") addList[it.key] =
                        it.value.plus(mainAdd[level])
                    else addList[it.key] = it.value.plus(subAdd[level])
                }
                "曲球型" -> {
                    if (it.key == "曲球" || it.key == "S曲球" || it.key == "K曲球") addList[it.key] =
                        it.value.plus(mainAdd[level])
                    else addList[it.key] = it.value.plus(subAdd[level])
                }
                "滑球型" -> {
                    if (it.key == "滑球" || it.key == "V滑球") addList[it.key] =
                        it.value.plus(mainAdd[level])
                    else addList[it.key] = it.value.plus(subAdd[level])
                }
                "二縫型" -> {
                    if (it.key == "V二縫" || it.key == "H二縫" || it.key == "切球") addList[it.key] =
                        it.value.plus(mainAdd[level])
                    else addList[it.key] = it.value.plus(subAdd[level])
                }
                "快速指叉球型" -> {
                    if (it.key == "指叉球" || it.key == "伸卡球" || it.key == "快速指叉球") addList[it.key] =
                        it.value.plus(mainAdd[level])
                    else addList[it.key] = it.value.plus(subAdd[level])
                }
                "特殊球種型" -> {
                    if (it.key == "螺旋球" || it.key == "掌心球" || it.key == "蝴蝶球") addList[it.key] =
                        it.value.plus(mainAdd[level])
                    else addList[it.key] = it.value.plus(subAdd[level])
                }
                "特殊曲球型" -> {
                    if (it.key == "滑曲球" || it.key == "12-6曲球" || it.key == "力量曲球") addList[it.key] =
                        it.value.plus(mainAdd[level])
                    else addList[it.key] = it.value.plus(subAdd[level])
                }
            }
        }

        return Pitcher(
            year,
            name,
            position,
            ovr,
            team,
            threw,
            hit,
            weather,
            type,
            stamina,
            control.plus(elseAdd[level]),
            speed.plus(elseAdd[level]),
            BallType1,
            BallType1Val.plus(mainAdd[level]),
            BallType2,
            addList[BallType2]!!,
            BallType3,
            addList[BallType3]!!,
            BallType4,
            addList[BallType4]!!,
            BallType5,
            addList[BallType5]!!,
            BallType6,
            addList[BallType6]!!
        )
    }
}