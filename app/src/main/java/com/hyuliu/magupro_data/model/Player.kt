package com.hyuliu.magupro_data.model

import android.os.Parcelable

interface Player : Parcelable {
    fun getPlayerTeam(): String
    fun getPlayerName(): String
    fun getPlayerYear(): String
    fun getPlayerPosition(): String
    fun getPlayerOVR(): Int
    fun getPlayerBP(): String
    fun getWeatherType():String
    fun getAbilityName1(): String
    fun getAbilityName2(): String
    fun getAbilityName3(): String
    fun getAbilityName4(): String
    fun getAbilityName5(): String
    fun getAbilityName6(): String
    fun getAbilityName7(): String
    fun getAbilityName8(): String
    fun getAbilityName9(): String
    fun getAbilityValue1(): String
    fun getAbilityValue2(): String
    fun getAbilityValue3(): String
    fun getAbilityValue4(): String
    fun getAbilityValue5(): String
    fun getAbilityValue6(): String
    fun getAbilityValue7(): String
    fun getAbilityValue8(): String
    fun getAbilityValue9(): String
    fun upGrade(level:Int):Player
}