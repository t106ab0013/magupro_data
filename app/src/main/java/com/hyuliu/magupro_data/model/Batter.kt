package com.hyuliu.magupro_data.model

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
data class Batter(
    val year: Int,
    val name: String,
    val position: String,
    val ovr: Int,
    val team: String,
    val threw: String,
    val hit: String,
    val weather: String,
    val type: String,
    val power: Int,
    val contact: Int,
    val runSpeed: Int,
    val defSpeed: Int,
    val defRange: Int,
    val threwPower: Int,
    val threwSkill: Int,
    val agile: Int
) : Parcelable, Player {
    override fun getPlayerTeam(): String {
        return team
    }

    override fun getPlayerName(): String {
        return name
    }

    override fun getPlayerYear(): String {
        return year.toString().substring(2)
    }

    override fun getPlayerPosition(): String {
        return position
    }

    override fun getPlayerOVR(): Int {
        return ovr
    }

    override fun getPlayerBP(): String {
        return PlayerDataMap.BATTER
    }

    override fun getWeatherType(): String {
        return weather
    }

    override fun getAbilityName1(): String {
        return "力量"
    }

    override fun getAbilityName2(): String {
        return "擊球"
    }

    override fun getAbilityName3(): String {
        return "跑速"
    }

    override fun getAbilityName4(): String {
        return "守速"
    }

    override fun getAbilityName5(): String {
        return "守範"
    }

    override fun getAbilityName6(): String {
        return "傳力"
    }

    override fun getAbilityName7(): String {
        return "傳技"
    }

    override fun getAbilityName8(): String {
        return "敏捷"
    }

    override fun getAbilityName9(): String {
        return PlayerDataMap.NONE
    }

    override fun getAbilityValue1(): String {
        return power.toString()
    }

    override fun getAbilityValue2(): String {
        return contact.toString()
    }

    override fun getAbilityValue3(): String {
        return runSpeed.toString()
    }

    override fun getAbilityValue4(): String {
        return defSpeed.toString()
    }

    override fun getAbilityValue5(): String {
        return defRange.toString()
    }

    override fun getAbilityValue6(): String {
        return threwPower.toString()
    }

    override fun getAbilityValue7(): String {
        return threwSkill.toString()
    }

    override fun getAbilityValue8(): String {
        return agile.toString()
    }

    override fun getAbilityValue9(): String {
        return "0"
    }

    override fun upGrade(level: Int): Player {
        val elseAdd: IntArray
        val agileAdd: IntArray
        if (ovr >= 81) {
            elseAdd = intArrayOf(0, 2, 3, 5, 6, 8, 9, 10, 12, 14, 18)
            agileAdd = intArrayOf(0, 1, 2, 3, 4, 6, 8, 9, 10, 12, 16)
        } else {
            elseAdd = intArrayOf(0, 2, 3, 4, 5, 6, 7, 8, 9, 11, 14)
            agileAdd = intArrayOf(0, 1, 2, 3, 4, 5, 7, 8, 9, 11, 14)
        }
        val mainAdd = arrayOf(0, 2, 3, 5, 7, 9, 11, 14, 17, 21, 26)
        val subAdd = arrayOf(0, 2, 3, 4, 5, 7, 9, 11, 13, 16, 20)
        val upGradeList = LinkedHashMap<String, Int>()

        if (type.contains("力量")) {
            upGradeList[getAbilityName1()] = power + mainAdd[level]
            upGradeList[getAbilityName2()] = contact + elseAdd[level]
            if (type.contains("速度")) {
                if (type.contains("A型")) {
                    upGradeList[getAbilityName3()] = runSpeed + mainAdd[level]
                    upGradeList[getAbilityName4()] = defSpeed + subAdd[level]
                } else {
                    upGradeList[getAbilityName3()] = runSpeed + subAdd[level]
                    upGradeList[getAbilityName4()] = defSpeed + mainAdd[level]
                }
                upGradeList[getAbilityName5()] = defRange + elseAdd[level]
                upGradeList[getAbilityName6()] = threwPower + elseAdd[level]
                upGradeList[getAbilityName7()] = threwSkill + elseAdd[level]
            } else if (type.contains("守備")) {
                if (type.contains("A型")) {
                    upGradeList[getAbilityName4()] = defSpeed + subAdd[level]
                    upGradeList[getAbilityName5()] = defRange + mainAdd[level]
                    upGradeList[getAbilityName6()] = threwPower + elseAdd[level]
                } else {
                    upGradeList[getAbilityName4()] = defSpeed + elseAdd[level]
                    upGradeList[getAbilityName5()] = defRange + mainAdd[level]
                    upGradeList[getAbilityName6()] = threwPower + subAdd[level]
                }
                upGradeList[getAbilityName3()] = runSpeed + elseAdd[level]
                upGradeList[getAbilityName7()] = threwSkill + elseAdd[level]
            } else if (type.contains("傳球")) {
                if (type.contains("A型")) {
                    upGradeList[getAbilityName6()] = threwPower + mainAdd[level]
                    upGradeList[getAbilityName7()] = threwSkill + subAdd[level]
                } else {
                    upGradeList[getAbilityName6()] = threwPower + subAdd[level]
                    upGradeList[getAbilityName7()] = threwSkill + mainAdd[level]
                }
                upGradeList[getAbilityName3()] = runSpeed + elseAdd[level]
                upGradeList[getAbilityName4()] = defSpeed + elseAdd[level]
                upGradeList[getAbilityName5()] = defRange + elseAdd[level]
            }
        } else {
            upGradeList[getAbilityName1()] = power + elseAdd[level]
            upGradeList[getAbilityName2()] = contact + mainAdd[level]
            if (type.contains("速度")) {
                if (type.contains("A型")) {
                    upGradeList[getAbilityName3()] = runSpeed + mainAdd[level]
                    upGradeList[getAbilityName4()] = defSpeed + subAdd[level]
                } else {
                    upGradeList[getAbilityName3()] = runSpeed + subAdd[level]
                    upGradeList[getAbilityName4()] = defSpeed + mainAdd[level]
                }
                upGradeList[getAbilityName5()] = defRange + elseAdd[level]
                upGradeList[getAbilityName6()] = threwPower + elseAdd[level]
                upGradeList[getAbilityName7()] = threwSkill + elseAdd[level]
            } else if (type.contains("守備")) {
                if (type.contains("A型")) {
                    upGradeList[getAbilityName4()] = defSpeed + subAdd[level]
                    upGradeList[getAbilityName5()] = defRange + mainAdd[level]
                    upGradeList[getAbilityName6()] = threwPower + elseAdd[level]
                } else {
                    upGradeList[getAbilityName4()] = defSpeed + elseAdd[level]
                    upGradeList[getAbilityName5()] = defRange + mainAdd[level]
                    upGradeList[getAbilityName6()] = threwPower + subAdd[level]
                }
                upGradeList[getAbilityName3()] = runSpeed + elseAdd[level]
                upGradeList[getAbilityName7()] = threwSkill + elseAdd[level]
            } else if (type.contains("傳球")) {
                if (type.contains("A型")) {
                    upGradeList[getAbilityName6()] = threwPower + mainAdd[level]
                    upGradeList[getAbilityName7()] = threwSkill + subAdd[level]
                } else {
                    upGradeList[getAbilityName6()] = threwPower + subAdd[level]
                    upGradeList[getAbilityName7()] = threwSkill + mainAdd[level]
                }
                upGradeList[getAbilityName3()] = runSpeed + elseAdd[level]
                upGradeList[getAbilityName4()] = defSpeed + elseAdd[level]
                upGradeList[getAbilityName5()] = defRange + elseAdd[level]
            }
        }

        return Batter(
            year,
            name,
            position,
            ovr,
            team,
            threw,
            hit,
            weather,
            type,
            upGradeList[getAbilityName1()]!!,
            upGradeList[getAbilityName2()]!!,
            upGradeList[getAbilityName3()]!!,
            upGradeList[getAbilityName4()]!!,
            upGradeList[getAbilityName5()]!!,
            upGradeList[getAbilityName6()]!!,
            upGradeList[getAbilityName7()]!!,
            agile + agileAdd[level]
        )
    }

    fun isTopDefSpeed(): Boolean {
        return if (type.contains("速度B型")) {
            defSpeed >= 85
        } else if (type.contains("速度A型") || type.contains("守備A型")) {
            defSpeed >= 91
        } else {
            if (ovr >= 81) {
                defSpeed >= 93
            } else {
                defSpeed >= 97
            }
        }
    }

    fun isTopRunSpeed(): Boolean {
        return if (type.contains("速度A型")) {
            defSpeed >= 85
        } else if (type.contains("速度B型")) {
            defSpeed >= 91
        } else {
            if (ovr >= 81) {
                defSpeed >= 93
            } else {
                defSpeed >= 97
            }
        }
    }


}