package com.hyuliu.magupro_data.kernel

import android.app.AlertDialog
import android.content.Context
import android.content.pm.PackageManager
import android.net.ConnectivityManager
import android.os.Build
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.widget.Button
import android.widget.CheckBox
import android.widget.Spinner
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import com.hyuliu.magupro_data.BuildConfig
import com.hyuliu.magupro_data.R
import com.hyuliu.magupro_data.adapter.SpinnerAdapter
import com.hyuliu.magupro_data.agent.FragmentEventCenter
import com.hyuliu.magupro_data.viewModel.MainViewModel
import com.hyuliu.magupro_data.viewModel.VMFactory


class MainActivity : AppCompatActivity(), View.OnClickListener {
    lateinit var toolbar: ConstraintLayout
    private lateinit var btnBack: Button
    private lateinit var tvTitle: TextView
    private lateinit var btnMore: Button
    private lateinit var btnSetting: Button
    val viewModel by lazy { VMFactory.viewModel(this, MainViewModel::class.java) }
    private val loadingDialog: AlertDialog by lazy {
        AlertDialog.Builder(this).setView(R.layout.progress_dialog1).create()
    }
    private val tvVersion:TextView by lazy { findViewById(R.id.tv_version) }
    private val versionName = BuildConfig.VERSION_NAME

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        toolbar = findViewById(R.id.toolbar)
        btnBack = findViewById(R.id.btn_back)
        btnMore = findViewById(R.id.btn_more)
        btnSetting = findViewById(R.id.btn_setting)
        tvTitle = findViewById(R.id.tv_title)
        tvVersion.text = versionName
        //初始畫面先載入首頁
        FragmentEventCenter.ChangeFragment(
            this.supportFragmentManager,
            FragmentEventCenter.Name.Search
        )
        initDialog()

        btnBack.setOnClickListener(this)
        btnMore.setOnClickListener(this)
        btnSetting.setOnClickListener(this)

        if (ContextCompat.checkSelfPermission(
                this,
                android.Manifest.permission.ACCESS_NETWORK_STATE
            ) != PackageManager.PERMISSION_GRANTED
        ) {
            ActivityCompat.requestPermissions(
                this,
                arrayOf(android.Manifest.permission.ACCESS_NETWORK_STATE),
                1
            )
        }


    }

    override fun onBackPressed() {
        if (supportFragmentManager.backStackEntryCount > 1) {
            this.supportFragmentManager.popBackStack()
        } else return
    }

    private fun initDialog(){
        loadingDialog.setCancelable(false)
    }

    fun setToolBarVisibility(visibility: Int) {
        toolbar.visibility = visibility
    }

    fun setbtnBackVisibility(visibility: Int) {
        btnBack.visibility = visibility
    }

    fun setTopbarTitle(title: String) {
        tvTitle.text = title
    }

    fun setBtnMoreVisibility(visibility: Int) {
        btnMore.visibility = visibility
        btnSetting.visibility = View.GONE
    }

    fun setBtnSettingVisibility(visibility: Int) {
        btnMore.visibility = View.GONE
        btnSetting.visibility = visibility
    }

    fun showLoadingDialog() {
        loadingDialog.show()
    }

    fun dismissLoadingDialog() {
        loadingDialog.dismiss()
    }

    override fun onClick(v: View?) {
        when (v) {
            btnBack -> {
                this.supportFragmentManager.popBackStack()
            }
            btnMore -> {
                FragmentEventCenter.ChangeFragment(
                    this.supportFragmentManager,
                    FragmentEventCenter.Name.About
                )
            }
            btnSetting -> {
                val view = LayoutInflater.from(this).inflate(R.layout.dialog_setting, null)
                val btnConfirm: Button by lazy { view.findViewById(R.id.btn_confirm) }
                val spLevel: Spinner by lazy { view.findViewById(R.id.spinner_level) }
                val cbRunSpeed: CheckBox by lazy { view.findViewById(R.id.cb_rsover120) }
                val cbDefSpeed: CheckBox by lazy { view.findViewById(R.id.cb_defover120) }
                val dialog = AlertDialog.Builder(this)
                    .setView(view)
                    .show()
                val adapter = SpinnerAdapter(this,this.resources.getStringArray(R.array.level))
                spLevel.adapter = adapter
                spLevel.setSelection(viewModel.filter.value!!.level)
                cbDefSpeed.isChecked = viewModel.filter.value!!.defSpeedOver120
                cbRunSpeed.isChecked = viewModel.filter.value!!.runSpeedOver120
                btnConfirm.setOnClickListener {
                    showLoadingDialog()
                    viewModel.setFilterValue(spLevel.selectedItemPosition,cbDefSpeed.isChecked,cbRunSpeed.isChecked)
                    dialog.dismiss()
                }
            }
        }
    }

    fun isNetworkConnected(): Boolean {
        return if (Build.VERSION.SDK_INT <= Build.VERSION_CODES.M) {
            val conManager = getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
            val internetInfo = conManager.activeNetworkInfo
            internetInfo != null && internetInfo.isConnected
        } else {
            var isConnect = false
            val connManager = getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
            val currentNetwork = connManager.activeNetwork
            if (currentNetwork != null) isConnect = true
            isConnect
        }
    }

}
