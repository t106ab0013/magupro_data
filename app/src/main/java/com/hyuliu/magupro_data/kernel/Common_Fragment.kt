package com.hyuliu.magupro_data.kernel

import android.os.Bundle
import android.view.View
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager

open class Common_Fragment:Fragment() {
    lateinit var mainActivity: MainActivity
    lateinit var fragmentEventCenter:FragmentManager

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        mainActivity = activity as MainActivity
        fragmentEventCenter = mainActivity.supportFragmentManager
        val argument = arguments
    }

    override fun onStart() {
        super.onStart()
        mainActivity.setbtnBackVisibility(View.VISIBLE)
        setBtnMoreVisibility(View.VISIBLE)
    }

    override fun onStop() {
        super.onStop()
        mainActivity.setTopbarTitle("")
    }

    fun setBtnMoreVisibility(visibility:Int){
        mainActivity.setBtnMoreVisibility(visibility)
    }

    fun setBtnSettingVisibility(visibility:Int){
        mainActivity.setBtnSettingVisibility(visibility)
    }

    fun showLoadingDialog(){
        mainActivity.showLoadingDialog()
    }

    fun dismissLoadingDialog(){
        mainActivity.dismissLoadingDialog()
    }

    fun isNetworkConnected():Boolean{
        return mainActivity.isNetworkConnected()
    }

}