package com.hyuliu.magupro_data.widget

import android.annotation.SuppressLint
import android.content.Context
import android.util.AttributeSet
import android.view.Gravity

class AbilityTextView : androidx.appcompat.widget.AppCompatTextView {
    constructor(context: Context) :super(context)
    constructor(context: Context, attrs: AttributeSet?) : super(context, attrs)
    constructor(context: Context, attrs: AttributeSet?, style: Int) : super(context, attrs, style)

    @SuppressLint("ResourceAsColor")
    override fun setText(text: CharSequence?, type: BufferType?) {
        super.setText(text, type)
        gravity = Gravity.CENTER
    }

}